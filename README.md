
# Prerequisitos #

* Framework Microsoft .NET 4.5
* NuGet packages:
	* Newtonsoft.Json (https://github.com/JamesNK/Newtonsoft.Json)
	* PdfiumViewer (https://github.com/pvginkel/PdfiumViewer)
	* MimeTypeMap (https://github.com/samuelneff/MimeTypeMap)

### Impresion de archivos ###

* La api chequea en https://v1.intrasistema.com/test_bridge_queue.php la existencia (o no) de un JSON con las siguientes propiedades: 
	* jobID (id del archivo)
	* file (el archivo en base64)
	* mime (el tipo de archivo)
	* printer (nombre de la impresora que va a realizar el trabajo)
	
*Ejemplo: 
	{	
		"file": {
				"jobID": "aaa001",
				"file": "SG9sYU11bmRvIA==",
				"mime": "application/pdf",
				"printer": "HP Laserjet 430"
			}
	}

### Envío de información de impresoras ###

* La información de las impresoras instaladas se envía a https://v1.intrasistema.com/test_bridge_printer_status.php en formato JSON

*Ejemplo: 
	{	
	  "Microsoft XPS Document Writer": {
				"Attributes": 576,
				"Availability": null,
				"AvailableJobSheets": null,
				"AveragePagesPerMinute": 0,
			}
	}