﻿using Microsoft.Win32;
using System;
using System.Windows.Forms;

namespace PrintCoupon
{
    public partial class frmLogin : Form
    {
        Principal principal = new Principal();

        public frmLogin()
        {
            InitializeComponent();
        }

        private void btnIngresar_Click(object sender, EventArgs e)
        {
            if (txtCredential.Text != "")
            {
                createRegistry(txtCredential.Text);
                this.Hide();
                principal.Show();
            }
            else
            {
                MessageBox.Show("Por favor, ingrese la key recibida");
            }

        }

        private static bool createRegistry(string key)
        {
            RegistryKey registryKey;
            registryKey = Registry.CurrentUser.CreateSubKey("Bridge");
            registryKey.SetValue("Key", key);
            registryKey.Close();

            return true;
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

        }
    }
}
