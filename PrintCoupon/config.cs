﻿using Newtonsoft.Json;
using System.Net;
using System;

namespace PrintCoupon
{
    public static class Config
    {
        private static string json_config_url = "https://v1.intrasistema.com/test_bridge_json.php";
        private static dynamic json_config;
        private static dynamic json_config_cache;

        private static string app_id;
        private static string instance_url;
        private static string printer_queue;
        private static string printers_status;
        private static string print_job_notify;

        private static int print_job_lookup_interval;
        private static int printer_status_push_interval;
        private static int internet_check_interval;
        private static int api_service_check_interval;

        public static Principal Principal;

        public static void writeLog(string message) {
            Principal.WriteLog(message);
        }

        public static dynamic Json_Config_
        {
            get { return json_config; }
            set { json_config = getJsonConfig(); }
        }

        public static string JsonConfigUrl_
        {
            get { return json_config_url; }
            set { json_config_url = value; }
        }

        public static string App_id_
        {
            get { return app_id; }
            set { app_id = value; }
        }

        public static string Instance_url_
        {
            get { return instance_url; }
            set { instance_url = value; }
        }

        public static string Printer_queue_
        {
            get { return printer_queue; }
            set { printer_queue = value; }
        }

        public static string Printers_status_
        {
            get { return printers_status; }
            set { printers_status = value; }
        }

        public static int Print_job_lookup_interval_
        {
            get { return print_job_lookup_interval; }
            set { print_job_lookup_interval = getPrintJobLookupInterval(); }
        }

        public static int Printer_status_push_interval_
        {
            get { return printer_status_push_interval; }
            set { printer_status_push_interval = value; }
        }

        public static string Print_job_notify_
        {
            get { return print_job_notify; }
            set { print_job_notify = value; }
        }

        public static int Internet_check_interval_
        {
            get { return internet_check_interval; }
            set { internet_check_interval = value; }
        }

        public static int Api_service_check_interval_
        {
            get { return api_service_check_interval; }
            set { api_service_check_interval = value; }
        }

        /*
         * Obtiene la configuracion inicial de manera remota
         */

        public static dynamic getJsonConfig()
        {
            if (json_config_cache == null) {
                try
                {
                    string conf = new WebClient().DownloadString(json_config_url);
                    json_config_cache = JsonConvert.DeserializeObject<dynamic>(conf);
                }
    #pragma warning disable CS0168 // Variable is declared but never used
                catch (Exception e)
    #pragma warning restore CS0168 // Variable is declared but never used
                {
                    throw new ServiceConfigExceptions(e.Message);
                }
            }

            return json_config_cache;
        }

        public static string getApp_id()
        {
             return getJsonConfig().app_id;
        }

        public static string getInstanceUrl()
        {
            writeLog("INSTANCE");
                return getJsonConfig().instance_url;
        }
        
        // QUEUE //
        public static string getPrinterQueue()
        {
            writeLog("URL QUEUE");
                return getJsonConfig().printer_queue_endpoint;
        }
        
        // PRINTER STATUS //
        public static string getPrinterStatus()
        {
                return getJsonConfig().printer_status_endpoint;
        }

        // NOTIFY PRINT JOB //
        public static string getNotifyPrintJob()
        {
            return getJsonConfig().status_print_job_url;
        }

        // URL PRINTERS INFO //
        public static string getUrlToSendPrintersInfo()
        {
            string instance = getInstanceUrl();
            string printerStatus = getPrinterStatus();
            return instance + printerStatus;

        }

        // URL PRINT JOB //
        public static string getUrlPrintJob()
        {
            writeLog("Obteniendo direccion total");
            return getInstanceUrl() + getPrinterQueue();
        }

        // URL NOTIFY PRINT JOB //
        public static string getUrlStatusPrintJob()
        {
            string instance = getInstanceUrl();
            string printNotify = getNotifyPrintJob();
            return instance + printNotify;
            
        }

        // INTERVALS //
        public static int getPrintJobLookupInterval()
        {
                return getJsonConfig().print_job_lookup_interval;
        }

        public static int getPrinterStatusPushiInterval()
        {
                return getJsonConfig().printer_status_push_interval;
        }

        public static int getInternetCheckInterval()
        {
                return getJsonConfig().internet_check_interval;   
        }

        public static int getApiServiceCheckInterval()
        {
                return getJsonConfig().api_service_check_interval; 
        }
    }
}
