﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintCoupon
{
    public class ServiceConfigExceptions : Exception
    {
        public ServiceConfigExceptions() : base() { }
        public ServiceConfigExceptions(string message) : base(message) { }
    }
}
