﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintCoupon
{
    class printJob
    {
        public IEnumerable<printJob> files { get; set; }
        public string file { get; set; }
        public string mime { get; set; }
        public string printer { get; set; }
        public string id { get; set; }
    }
}
