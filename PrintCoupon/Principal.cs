﻿using Microsoft.Win32;
using MimeTypes;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PdfiumViewer;
using System;
using System.Collections.Specialized;
using System.Drawing.Printing;
using System.IO;
using System.Management;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Windows.Forms;


namespace PrintCoupon
{
    public partial class Principal : Form
    {
        bool errorPrint = false;
        System.Windows.Forms.Timer printer_status_push_interval = new System.Windows.Forms.Timer();
        System.Windows.Forms.Timer print_job_lookup_interval = new System.Windows.Forms.Timer();
        System.Windows.Forms.Timer internet_check = new System.Windows.Forms.Timer();
        System.Windows.Forms.Timer api_service_check = new System.Windows.Forms.Timer();

        public Principal()
        {
            Config.Principal = this;
            InitializeComponent();
        }

        private void Principal_Load(object sender, EventArgs e)
        {
            CheckInternetConnection();
            rbtnInternet.Checked = true;
            WriteLog("Conexión a internet detectada.");

            CheckStatusApiService();
            rbtnApiService.Checked = true;
            WriteLog("Conectado a la API de iNTRASISTEMA.");

            InitializeIntervals();
            EnableIntervals();

            WriteLog("Buscando archivos para imprimir...");
        }

        private void EnableIntervals()
        {
            printer_status_push_interval.Enabled = true;
            print_job_lookup_interval.Enabled = true;
            internet_check.Enabled = true;
            api_service_check.Enabled = true;

            printer_status_push_interval.Tick += new EventHandler(PrinterStatusPushIntervalTick);
            print_job_lookup_interval.Tick += new EventHandler(print_job_lookup_interval_Tick);
            internet_check.Tick += new EventHandler(internet_check_Tick);
            api_service_check.Tick += new EventHandler(api_service_check_Tick);

            printer_status_push_interval.Start();
            print_job_lookup_interval.Start();
            internet_check.Start();
            api_service_check.Start();


        }

        private void InitializeIntervals()
        {
            while (!InitializeElements())
            {
                int milliseconds = 5000;
                AutoClosingMessageBox.Show("Existe un error en los datos devueltos por el servidor API.", "Error", 2000);
                Thread.Sleep(milliseconds);
            }
        }

        private void CheckStatusApiService()
        {
            WriteLog("Verificando estado de servicio...");
            while (ApiServiceStatus() == false)
            {
                AutoClosingMessageBox.Show("No hay conexion con el servidor API de iNTRASISTEMA.", "Error", 2000);
                int milliseconds = 5000;
                Thread.Sleep(milliseconds);
            }
        }

        private bool ApiServiceStatus()
        {
            try
            {
                Config.getJsonConfig();
                return true;
            }
            catch
            {
                return false;
            }

        }

        private void CheckInternetConnection()
        {
            while (!InternetConnection())
            {
                AutoClosingMessageBox.Show("No hay conexion a internet.", "Error", 2000);
                int milliseconds = 5000;
                Thread.Sleep(milliseconds);
            }
        }

        public void WriteLog(string message)
        {
            txtMensajes.Text += DateTime.Now.ToString() + "   |   " + message + "\r\n";
            scrollTextbox();
        }

        private bool InitializeElements()
        {
            try
            {
                print_job_lookup_interval.Interval = Config.getPrintJobLookupInterval();
                printer_status_push_interval.Interval = Config.getPrinterStatusPushiInterval();
                internet_check.Interval = Config.getInternetCheckInterval();
                api_service_check.Interval = Config.getApiServiceCheckInterval();
            }
#pragma warning disable CS0168 // Variable is declared but never used
            catch (ServiceConfigExceptions e)
#pragma warning restore CS0168 // Variable is declared but never used
            {
                // Ver que hacemos acá
                WriteLog("Whooops! ver que hacemos!");
            }

            // Progress bar
            backgroundWorker1.WorkerReportsProgress = true;
            backgroundWorker1.RunWorkerAsync();
            return true;
        }

        public OrderedDictionary getPrinters()
        {
            OrderedDictionary printers = new OrderedDictionary();
            foreach (string printer in PrinterSettings.InstalledPrinters)
            {
                printers.Add(printer, getPrinterInfo(printer));
            }

            OrderedDictionary jsonPrinters = new OrderedDictionary();
            jsonPrinters.Add("app_key", getAppKey());
            jsonPrinters.Add("printers", printers);
            return jsonPrinters;
        }

        private static OrderedDictionary getPrinterInfo(string printerName)
        {
            // GET INFO PRINTER

            string query = string.Format("SELECT * from Win32_Printer WHERE Name LIKE '%{0}'", printerName);

            ManagementObjectSearcher searcher = new ManagementObjectSearcher(query);
            ManagementObjectCollection coll = searcher.Get();

            OrderedDictionary printersInfo = new OrderedDictionary();
            foreach (ManagementObject printer in coll)
            {
                foreach (PropertyData property in printer.Properties)
                {
                    printersInfo.Add(property.Name, property.Value);
                }
            }
            return printersInfo;
        }

        public string PrintPDF(string printer, string filePath)
        {
            try
            {
                var document = PdfDocument.Load(filePath);
                var printDocument = document.CreatePrintDocument();
                printDocument.PrinterSettings.PrinterName = printer;
                printDocument.Print();

                WriteLog("Imprimiendo...");
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());
                errorPrint = true;
                return ex.ToString();
            }
            return DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        }

        private async void sendInfo(dynamic json, string urlUpload)
        {
            try
            {
                var client = new HttpClient();
                var response = await client.PostAsync(
                    urlUpload,
                    new StringContent(Convert.ToString(json), Encoding.UTF8, "application/json")
                );

                string resultContent = await response.Content.ReadAsStringAsync();
                WriteLog("Información enviada correctamente.");
            }
#pragma warning disable CS0168 // Variable is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // Variable is declared but never used
            {
                WriteLog("Hubo un error enviando la información, re-intentando.");
            }
        }

        private void getPrintJob(string url)
        {
            try
            {
                // DOWNLOAD JSON //
                WriteLog("bajando json");
                string printJson = new WebClient().DownloadString(url);
                WriteLog("convirtiendo json");
                var printJob = JsonConvert.DeserializeObject<dynamic>(printJson);

                WriteLog("obteniendo archivos");
                foreach (JToken result in printJob["files"])
                {
                    WriteLog("verificando impresora");
                    // VERIFY PRINTER //
                    if (!verifyPrinter(result["printer"].ToString()))
                    {
                        AutoClosingMessageBox.Show("La impresora " + result["printer"].ToString() + " no se encuentra disponible en el sistema", "Error", 2500);
                        // ENVIAR INFO DE ERROR //
                        continue;
                    }
                    WriteLog("mime type");
                    // GET MIMETYPE //
                    string mimeType = MimeTypeMap.GetExtension(result["mime"].ToString());
                    WriteLog("print job id");
                    // GET ID PRINT JOB //
                    string id = result["id"].ToString();
                    WriteLog("filepath");
                    // CREATE FILEPATH //
                    string filepath = Path.GetTempPath() + id + mimeType;
                    WriteLog("bajando archivo");
                    // GET FILE
                    WebClient wb = new WebClient();
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    try
                    {
                        wb.DownloadFile(result["url"].ToString(), filepath);
                        WriteLog("enviando a impresora");
                        // SEND TO PRINT //
                        string StatusPrint = PrintPDF(result["printer"].ToString(), filepath);
                        WriteLog("enviando info de impresion");
                        sendInfo(createPrinterJobStatus(StatusPrint, id, errorPrint), Config.getUrlStatusPrintJob());
                    }
                    catch
                    {
                        WriteLog("No se pudo bajar el archivo a imprimir");
                        continue;
                    }

                }
            }
#pragma warning disable CS0168 // Variable is declared but never used
            catch (WebException ex)
#pragma warning restore CS0168 // Variable is declared but never used
            {
                WriteLog("No hay nada para imprimir.");
            }
        }

        private bool verifyPrinter(string printer)
        {
            WriteLog("obteniendo impresoras");

            foreach (string printe in PrinterSettings.InstalledPrinters)
            {
                if (printe.Contains(printer))
                {
                    return true;
                }
            }
            return false;
        }

        private dynamic createPrinterJobStatus(string status, string id, bool statusPrint)
        {
            dynamic json = new JObject();
            json.appKey = getAppKey();
            json.id = id;
            json.error = statusPrint;
            json.printed_at = status;
            return json;
        }

        private string getAppKey()
        {
            RegistryKey key = Registry.CurrentUser.OpenSubKey("Bridge");
            return key.GetValue("Key").ToString();
        }

        private void scrollTextbox()
        {
            int lines = txtMensajes.Lines.Length;

            if (lines >= 100)
            {
                txtMensajes.Clear();
            }
            else
            {
                txtMensajes.SelectionStart = txtMensajes.Text.Length;
                txtMensajes.ScrollToCaret();
            }
        }

        private void backgroundWorker1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            for (int i = 1; i <= 120; i++)
            {
                Thread.Sleep(15);
                backgroundWorker1.ReportProgress(i, null);
            }
        }

        private void backgroundWorker1_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            progressBar1.Style = ProgressBarStyle.Marquee;
            progressBar1.Value = e.ProgressPercentage;
        }

        private void internet_check_Tick(object sender, EventArgs e)
        {
            if (InternetConnection() == true)
            {
                rbtnInternet.Checked = true;
                WriteLog("Conectado.");
            }
            else
            {
                rbtnInternet.Checked = false;
                WriteLog("NO HAY CONEXION.");
            }
        }

        public bool InternetConnection()
        {
            try
            {
                IPHostEntry host = Dns.GetHostEntry("www.google.com");
                return true;
            }
            catch
            {
                return false;
            }
        }

        private void api_service_check_Tick(object sender, EventArgs e)
        {
            if (ApiServiceStatus() == true)
            {
                rbtnApiService.Checked = true;
                WriteLog("Conectado a la API de iNTRASISTEMA.");
            }
            else
            {
                rbtnApiService.Checked = false;
                WriteLog("NO HAY CONEXION CON EL SERVIDOR.");
            }

        }

        // SEND PRINTERS INFO
        private void PrinterStatusPushIntervalTick(object sender, EventArgs e)
        {
            WriteLog("Enviando información de estado de impresoras.");
            OrderedDictionary printers = getPrinters();
            dynamic printerJson = JsonConvert.SerializeObject(printers, Formatting.Indented);
            string urlUpload = Config.getUrlToSendPrintersInfo();
            sendInfo(printerJson, urlUpload);
        }

        private void btnForzarClick(object sender, EventArgs e)
        {
            getPrintJob(Config.getUrlPrintJob());
            OrderedDictionary printers = getPrinters();
            dynamic printerJson = JsonConvert.SerializeObject(printers, Formatting.Indented);
            string urlUpload = Config.getUrlToSendPrintersInfo();
            sendInfo(printerJson, urlUpload);
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void configuracionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmConfig frmConfig = new frmConfig();
            frmConfig.Show();
        }

        // CHEQUEAR PRINT JOB
        private void print_job_lookup_interval_Tick(object sender, EventArgs e)
        {
            WriteLog("Buscando archivos para imprimir...");
            getPrintJob(Config.getUrlPrintJob());
        }

        // MINIMIZAR A LA BANDEJA DE NOTIFICACION
        private void Principal_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                this.Visible = false;
                notifyIcon1.ShowBalloonTip(3000);
                notifyIcon1.Visible = true;
            }
        }

        // MAXIMIZAR
        private void notifyIcon1_MouseClick(object sender, MouseEventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
            notifyIcon1.Visible = false;
        }

        private void acercaDeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            acercaDe frmAcercaDe = new acercaDe();
            frmAcercaDe.Show();
        }
    }
}
