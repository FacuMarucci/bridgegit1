﻿using System;
using System.Windows.Forms;

namespace PrintCoupon
{
    public partial class frmConfig : Form
    {
        public frmConfig()
        {
            InitializeComponent();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            Config.JsonConfigUrl_ = txtUrlV1.Text;
            this.Close();
        }

        private void frmConfig_Load(object sender, EventArgs e)
        {
            txtUrlV1.Text = Config.JsonConfigUrl_;
        }
    }
}
