﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Log;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace PrintCoupon
{
    static class Program
    {

        static Mutex _m;

        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.ThreadException += new ThreadExceptionEventHandler(Application_ThreadException);

            if (!Program.IsSingleInstance())
            {
                Application.Exit(); // Exit program.
            }
            else
            {
                verifingCredentials();
            }
        }
        static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            using (FileRecordSequence record = new FileRecordSequence("application.log", FileAccess.Write))
            {

                string message = string.Format("[{0}]Message::{1} StackTrace:: {2}", DateTime.Now,
                                                                                    e.Exception.Message,
                                                                                    e.Exception.StackTrace);

                record.Append(CreateData(message), SequenceNumber.Invalid,
                                                            SequenceNumber.Invalid,
                                                            RecordAppendOptions.ForceFlush);
            }
        }

        static bool IsSingleInstance()
        {
            try
            {
                // Try to open existing mutex.
                Mutex.OpenExisting("Bridge");
            }
            catch
            {
                // If exception occurred, there is no such mutex.
                Program._m = new Mutex(true, "Bridge");

                // Only one instance.
                return true;
            }
            // More than one instance.
            return false;
        }

        private static IList<ArraySegment<byte>> CreateData(string str)
        {
            Encoding enc = Encoding.Unicode;

            byte[] array = enc.GetBytes(str);

            ArraySegment<byte>[] segments = new ArraySegment<byte>[1];
            segments[0] = new ArraySegment<byte>(array);

            return Array.AsReadOnly<ArraySegment<byte>>(segments);
        }

        static void verifingCredentials()
        {
            RegistryKey key = Registry.CurrentUser.OpenSubKey("Bridge");
            if (key == null)
            {
                Application.Run(new frmLogin());
            }
            else
            {
                Object o = key.GetValue("Key");
                if (o != null)
                {
                    Application.Run(new Principal());
                }
                else
                {
                    Application.Run(new frmLogin());
                }
            }

        }

    }
}
